<?php
$output ='';
?>
<!DOCTYPE html>
<html lang="fr" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>MyAlbumList</title>
  <link rel="stylesheet" href="../static/liste.css"/>
</head>
<body>
<?php
include_once("header.php");
?>
  <section class="afficheListe">
    <?php
    if(isset($_POST['albums'])){
      echo "<h2>Voici les Albums :</h2>";
      $sql = "SELECT * from ALBUM";
      $query = mysqli_query($bd, $sql);
      ?>
      <form class="" action="info.php" method="post">
        <?php

        while ($row = mysqli_fetch_array($query)) {
          $res = $row['titleAl'];
          echo "<input type='submit' name='album' value='$res'> <br>";
        }
        ?>
      </form>
      <?php

    }
    if(isset($_POST['artistes'])){
      echo "<h2>Voici les Artistes :</h2>";
      $sql = "SELECT * from ARTIST";
      $query = mysqli_query($bd, $sql);
      ?>
      <form class="" action="info.php" method="post">
        <?php
        while ($row = mysqli_fetch_array($query)) {
          $res = $row['nameAr'];
          echo "<input type='submit' name='artiste' value='$res'> <br>";
        }
        ?>
      </form>
      <?php
    }
    if(isset($_POST['titres'])){
      echo "<h2>Voici les Titres :</h2>";
      $sql = "SELECT * from TRACK natural join ALBUM";
      $query = mysqli_query($bd, $sql);
      ?>
      <form class="" action="info.php" method="post">
        <?php
        while ($row = mysqli_fetch_array($query)) {
          $res_morceau = $row['titleTr'];
          $res_morceau_album = $row['titleAl'];
          echo "<input type='submit' name='track' value='$res_morceau ($res_morceau_album)'> <br>";
        }
        ?>
      </form>
      <?php
    }
    if(isset($_POST['genres'])){
      $output .= "<h2>Voici les Genres :</h2>";
      $sql = "SELECT * from GENRE";
      $query = mysqli_query($bd, $sql);
      while ($row = mysqli_fetch_array($query)) {
        $res = $row['nameGe'];
        $output .= '<p>'.$res.'</p>';
      }
    }

    echo $output;
    ?>


  </section>
<?php include_once("footer.html"); ?>
</body>
</html>
