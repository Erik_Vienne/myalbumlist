<?php
session_start();
$bd = mysqli_connect("localhost", "pi", "hulk26jeej", "mydb");
?>
<link rel="stylesheet" href="../static/header.css"/>
<header>
    <h2><a href="accueil.php">MyAlbumList</a></h2>
    <img id="search" src="../resources/search.png" alt="search logo" height="28" width="28">

    <form class="Recherche" action="recherche.php" method="post">
        <p>
            <input name="search" type="text" pattern="[A-Za-z- ]{1,}"
                   title="Entrer un mot clé de recherche" maxlength="100"
                   placeholder="Nom d'un artiste, d'un album,...">
            <input type="submit" name="rechercher" value="Rechercher">
        </p>
    </form>

    <?php if(isset($_SESSION['username'])){?>
        <img id="profile" src="../resources/profile.png" alt="search logo" height="28" width="28">
        <h1 id="nomUser"><?php echo $_SESSION['username'];?></h1>
        <a id="deco" href="logout.php">Se déconnecter</a>
    <?php }
    else{?>
        <a id="deco" href="login.php">Se connecter</a>
    <?php }
    ?>
</header>
<div>
</div>
<section id="navP">
    <form class="" action="liste.php" method="post">
        <table>
            <tr>
                <th><input id="listeheader" type="submit"  name="albums" value="Albums" /></th>
                <th><input id="listeheader" type="submit"  name="artistes" value="Artistes" /></th>
                <th><input id="listeheader" type="submit"  name="titres" value="Titres" /></th>
                <th><input id="listeheader" type="submit"  name="genres" value="Genres" /></th>
            </tr>
        </table>
    </form>
</section>
<div>
</div>
