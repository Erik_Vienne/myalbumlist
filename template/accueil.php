<!DOCTYPE html>
<html lang="fr">
  <head>
    <link rel="stylesheet" href="../static/accueil.css"/>
    <meta charset="utf-8">
    <title>Bienvenue sur MAL</title>
  </head>
  <body>
  <?php
  include_once("header.php");

    if(isset($_SESSION['username'])){
      $username = $_SESSION['username'];
      $sql_id = "SELECT * from USER where nameUs = '$username'";
      $query_id = mysqli_query($bd, $sql_id);
      while ($row = mysqli_fetch_array($query_id)) {
        $id_us = $row['idUs'];
      }
      $sql_has = "SELECT * from HAS where idUs = '$id_us'";
      $query_has = mysqli_query($bd, $sql_has);
      $count = mysqli_num_rows($query_has);


      if($count == 0){
        echo 'Vous n\'avez pas encore ajouté d\'albums...';
      }
      else {
        echo "Vous avez : <br>";
        ?>
        <form class="" action="info.php" method="post">
          <?php
          while ($row_has = mysqli_fetch_array($query_has)) {
            $id_al = $row_has['idAl'];
            $sql_album = "SELECT * from ALBUM where idAl = '$id_al'";
            $query_album = mysqli_query($bd, $sql_album);
            $mark = $row_has['mark'];
            while ($row_album = mysqli_fetch_array($query_album)) {
              $nom_album = $row_album['titleAl'];
              echo "<input type='submit' name='album' value='$nom_album'>, noté $mark sur 5 <br>";
            }
          }
           ?>
        </form>
        <?php
      }
    }
    else{
      echo "<a href='login.php'>Connectez-vous pour voir et ajouter vos albums</a>";
    }
    include_once("footer.html");
     ?>
  </body>
</html>
