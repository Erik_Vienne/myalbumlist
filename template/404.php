<?php
  $addr = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
  $expl = explode("/myalbumlist/template/404.php", $addr);
  if(!end($expl)==""){
    $root = explode("/myalbumlist/", $addr);
    $addr404 = reset($root)."/myalbumlist/template/404.php";
    header("location: ".$addr404);
  }
  session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
  <link rel="stylesheet" href="../static/info.css"/>
  <meta charset="utf-8">
  <title>Page introuvable</title>
</head>
<body>
  <header>
    <h2><a href="accueil.php">MyAlbumList</a></h2>
    <img id="search" src="../resources/search.png" alt="search logo" height="28" width="28">

    <form class="Recherche" action="recherche.php" method="post">
      <p>
        <input name="search" type="text" pattern="[A-Za-z- ]{1,}"
        title="Entrer un mot clé de recherche" maxlength="100"
        placeholder="Nom d'un artiste, d'un album,...">
        <input type="submit" name="rechercher" value="Rechercher">
      </p>
    </form>

    <?php if(isset($_SESSION['username'])){?>
      <img id="profile" src="../resources/profile.png" alt="search logo" height="28" width="28">
      <h1 id="nomUser"><?php echo $_SESSION['username'];?></h1>
      <a id="deco" href="logout.php">Se déconnecter</a>
    <?php }
    else{?>
      <a id="deco" href="login.php">Se connecter</a>
    <?php }
    ?>
  </header>
  <div id="gauche">

  </div>
  <section id= "info">
    <h1>Page introuvable</h1>
    <h3><a href="accueil.php">Retour à l'acceuil</a></h2>

  </section>
  <div id="droite">

  </div>
  <div id="pushFooter">
  </div>
  <footer>
    <section>

      <h2>Information</h2>
      <p>Site créé par Sosthène Leroy, Erik Vienne et Thomas Quetier dans le cadre d'un projet d'étude</p>
      <p>Pour tout contact : <a href="mailto:sosthene.leroy@etu.univ-orleans.fr">sosthene.leroy@etu.univ-orleans.fr</a></p>

    </section>

    <img src="../resources/logoIUT.png" alt="IUT orleans" height="100" width="150">
  </footer>
</body>
</html>
